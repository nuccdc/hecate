{
  description = "hecate";
  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "hecate";
      buildInputs = with pkgs; [
        postgresql.lib
        rustup
        rust-analyzer
      ];
      shellHook = ''
        rustup default stable

        rustup component add cargo
        rustup component add rustc
        rustup component add rust-std
        rustup component add rust-src

        rustup target add wasm32-unknown-unknown

        export PATH=$PATH:~/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin:~/.cargo/bin
        which sea-orm-cli || cargo install sea-orm-cli --no-default-features --features runtime-tokio-rustls,codegen
        which cargo-watch || cargo install cargo-watch
        which trunk || cargo install trunk
        export DATABASE_URL="postgres://postgres:postgres@localhost/hecate"
      '';
    };
  };
}
