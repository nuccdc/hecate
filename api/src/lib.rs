use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Me {
    pub name: String,
    pub team: bool,
    pub applicant: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Applicant {
    pub name: String,
    pub picture: String,
    pub progress: Vec<String>,
    pub time_used_percentage: u8,
}
