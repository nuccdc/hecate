use dioxus::prelude::*;

use crate::{appearance::use_theme, use_fetch};

#[allow(non_snake_case)]
pub fn TeamHome(cx: Scope) -> Element {
    let res = use_fetch::<Vec<api::Applicant>, _>(&cx, "/api/applicants");
    match res.value() {
        None => cx.render(rsx!(
            h1 { "Applicants" },
            span { "Loading..." }
        )),
        Some(Err(crate::FetchError::Unauthorized)) => cx.render(rsx!(Redirect { to: "/" })),
        Some(Err(e)) => cx.render(rsx!(
            h1 { "Applicants" },
            span { "Error: {e:?}" }
        )),
        Some(Ok(applicants)) => cx.render(rsx!(
            h1 { "Applicants" },
            div {
                class: "row",
                applicants.iter().map(|a| {
                    rsx!(
                        Applicant {
                            key: "{a.name}-{a.picture}",
                            applicant: a.clone(),
                        }
                    )
                })
            }
        )),
    }
}

#[derive(Props, PartialEq)]
struct ApplicantProps {
    applicant: api::Applicant,
}

#[allow(non_snake_case)]
fn Applicant(cx: Scope<ApplicantProps>) -> Element {
    let theme = use_theme(&cx);
    let applicant = &cx.props.applicant;
    cx.render(rsx!(
        div {
            class: "col-sm-3",
            div {
                class: "my-2 card bg-{theme.bg} text-{theme.text} border-secondary animate-colors",
                div {
                    class: "card-body",
                    div {
                        class: "card-title h5",
                        img {
                            alt: "{applicant.name}'s status",
                            class: "rounded",
                            src: "{applicant.picture}",
                            width: "50",
                            height: "50",
                        }
                        span {
                            class: "mx-2",
                            "{applicant.name}"
                        }
                    }
                    div {
                        class: "card-text",
                        h6 { "Completion status:" }
                        ul {
                            applicant.progress.iter().map(|p| {rsx!(
                                li { key: "{applicant.name}-{p}", "{p}" }
                            )})
                        }
                    }
                    div {
                        class: "progress",
                        div {
                            role: "progressbar",
                            class: "progress-bar bg-primary",
                            style: "width: {applicant.time_used_percentage}%",
                            "{applicant.time_used_percentage}% of time used"
                        }
                    }
                }
            }
        }
    ))
}
