use appearance::use_theme;
use dioxus::prelude::*;
use reqwasm::http::Request;
use serde::de::DeserializeOwned;

mod appearance;
mod applicant;
mod nav;
mod team;

static LOGIN_INFO: Atom<Option<api::Me>> = |_| None;

#[allow(non_snake_case)]
fn Home(cx: Scope) -> Element {
    let login_info = use_read(&cx, LOGIN_INFO);
    match login_info {
        None => cx.render(rsx!(NotLoggedIn {})),
        Some(info) if info.team => cx.render(rsx!(Redirect { to: "/applicants" })),
        Some(info) if info.applicant => cx.render(rsx!(Redirect { to: "/apply" })),
        Some(_) => cx.render(
            rsx!(p { "You are neither a team member nor an applicant.. Please contact an admin."}),
        ),
    }
}

#[allow(non_snake_case)]
fn NotLoggedIn(cx: Scope) -> Element {
    cx.render(rsx!(
        div {
            class: "mt-3 row",
            form {
                class: "col",
                action: "/auth/login",
                method: "POST",
                button {
                    class: "btn btn-primary",
                    "type": "submit",
                    "Login"
                }
            }
        }
    ))
}

#[allow(non_snake_case)]
fn FourOhFour(cx: Scope) -> Element {
    cx.render(rsx!(
        h1 { "Page not found" }
        "Could not find the requested page."
    ))
}

#[derive(Debug)]
enum FetchError {
    RequestError(reqwasm::Error),
    Unauthorized,
    JsonError(reqwasm::Error),
}

type FetchFuture<T> = UseFuture<Result<T, FetchError>>;

fn use_fetch<'a, T, S>(cx: &'a Scope<S>, url: &'static str) -> &'a FetchFuture<T>
where
    T: DeserializeOwned,
    T: 'static,
{
    use_future(&cx, (&url,), |_| async move {
        let res = match Request::get(url).send().await {
            Ok(res) => res,
            Err(e) => return Err(FetchError::RequestError(e)),
        };
        if res.status() == 401 {
            return Err(FetchError::Unauthorized);
        }
        match res.json::<T>().await {
            Ok(val) => Ok(val),
            Err(e) => return Err(FetchError::JsonError(e)),
        }
    })
}

fn app(cx: Scope) -> Element {
    let set_login_info = use_set(&cx, LOGIN_INFO);
    let res = use_fetch::<api::Me, _>(&cx, "/api/me");
    match res.value() {
        Some(Ok(me)) => {
            log::debug!("Got user with name [{}]", me.name);
            set_login_info(Some(me.clone()));
            res.cancel(&cx);
        }
        Some(Err(e)) => {
            log::error!("There was an error hitting the API: {:?}", e);
        }
        None => {}
    }
    let theme = use_theme(&cx);
    cx.render(rsx!(
        div {
            class: "w-100 h-100 {theme.bg_class} {theme.text_class} animate-colors",
            nav::Nav {}
            div {
                class: "container",
                Router {
                    Route { to: "/", Home {} }
                    Route { to: "/applicants", team::TeamHome {} }
                    Route { to: "/apply", applicant::ApplicantHome {} }
                    Route { to: "", FourOhFour {} }
                }
            }
        }
    ))
}

const LOGO: &str = "
NNNNNNNN        NNNNNNNNUUUUUUUU     UUUUUUUU       CCCCCCCCCCCCC       CCCCCCCCCCCCCDDDDDDDDDDDDD                CCCCCCCCCCCCC
N:::::::N       N::::::NU::::::U     U::::::U    CCC::::::::::::C    CCC::::::::::::CD::::::::::::DDD          CCC::::::::::::C
N::::::::N      N::::::NU::::::U     U::::::U  CC:::::::::::::::C  CC:::::::::::::::CD:::::::::::::::DD      CC:::::::::::::::C
N:::::::::N     N::::::NUU:::::U     U:::::UU C:::::CCCCCCCC::::C C:::::CCCCCCCC::::CDDD:::::DDDDD:::::D    C:::::CCCCCCCC::::C
N::::::::::N    N::::::N U:::::U     U:::::U C:::::C       CCCCCCC:::::C       CCCCCC  D:::::D    D:::::D  C:::::C       CCCCCC
N:::::::::::N   N::::::N U:::::D     D:::::UC:::::C             C:::::C                D:::::D     D:::::DC:::::C
N:::::::N::::N  N::::::N U:::::D     D:::::UC:::::C             C:::::C                D:::::D     D:::::DC:::::C
N::::::N N::::N N::::::N U:::::D     D:::::UC:::::C             C:::::C                D:::::D     D:::::DC:::::C
N::::::N  N::::N:::::::N U:::::D     D:::::UC:::::C             C:::::C                D:::::D     D:::::DC:::::C
N::::::N   N:::::::::::N U:::::D     D:::::UC:::::C             C:::::C                D:::::D     D:::::DC:::::C
N::::::N    N::::::::::N U:::::D     D:::::UC:::::C             C:::::C                D:::::D     D:::::DC:::::C
N::::::N     N:::::::::N U::::::U   U::::::U C:::::C       CCCCCCC:::::C       CCCCCC  D:::::D    D:::::D  C:::::C       CCCCCC
N::::::N      N::::::::N U:::::::UUU:::::::U  C:::::CCCCCCCC::::C C:::::CCCCCCCC::::CDDD:::::DDDDD:::::D    C:::::CCCCCCCC::::C
N::::::N       N:::::::N  UU:::::::::::::UU    CC:::::::::::::::C  CC:::::::::::::::CD:::::::::::::::DD      CC:::::::::::::::C
N::::::N        N::::::N    UU:::::::::UU        CCC::::::::::::C    CCC::::::::::::CD::::::::::::DDD          CCC::::::::::::C
NNNNNNNN         NNNNNNN      UUUUUUUUU             CCCCCCCCCCCCC       CCCCCCCCCCCCCDDDDDDDDDDDDD                CCCCCCCCCCCCC
";

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    log::info!("{}", LOGO);
    log::info!("Welcome to Hecate.");
    dioxus::web::launch(app);
}
