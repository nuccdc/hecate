use dioxus::prelude::*;

#[allow(non_snake_case)]
pub fn ApplicantHome(cx: Scope) -> Element {
    cx.render(rsx!(
        h1 { "Applicant Home" }
    ))
}
