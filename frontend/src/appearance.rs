use std::rc::Rc;

use dioxus::prelude::*;

pub struct Theme {
    pub bg: &'static str,
    pub bg_class: &'static str,
    pub text: &'static str,
    pub text_class: &'static str,
    pub switch_sym: &'static str,
}

impl Theme {
    fn light() -> Self {
        Theme {
            bg: "light",
            bg_class: "bg-light",
            text: "black",
            text_class: "text-dark",
            switch_sym: "Switch to dark mode",
        }
    }

    fn dark() -> Self {
        Theme {
            bg: "dark",
            bg_class: "bg-dark",
            text: "white",
            text_class: "text-white",
            switch_sym: "Switch to light mode",
        }
    }
}

// TODO: persist to local storage
static THEME: Atom<Theme> = |_| Theme::dark();

pub fn use_theme<'a, T>(cx: &Scope<'a, T>) -> &'a Theme {
    return use_read(&cx, THEME);
}

fn use_theme_toggle<'a, T>(cx: &Scope<'a, T>) -> Rc<dyn Fn() + 'a> {
    let toggle = use_state(&cx, || true);
    let set_theme = use_set(&cx, THEME);
    let toggle_bool: bool = **toggle;
    let ret: Rc<dyn Fn() + 'a> = Rc::new(move || {
        if toggle_bool {
            set_theme(Theme::light());
        } else {
            set_theme(Theme::dark());
        }
        toggle.set(!toggle_bool);
    });
    ret
}

#[derive(Props)]
pub struct ThemeToggleButtonProps<'a> {
    class: &'a str,
}

#[allow(non_snake_case)]
pub fn ThemeToggleButton<'a>(cx: Scope<'a, ThemeToggleButtonProps>) -> Element<'a> {
    let theme = use_theme(&cx);
    let toggle = use_theme_toggle(&cx);
    let class = cx.props.class;
    cx.render(rsx!(
        button {
            class: "btn btn-primary {class}",
            onclick: move |_| toggle(),
            "{theme.switch_sym}"
        }
    ))
}
