use dioxus::{fermi::use_atom_state, prelude::*};

use crate::{
    appearance::{self, ThemeToggleButton},
    LOGIN_INFO,
};

#[allow(non_snake_case)]
pub fn Nav(cx: Scope) -> Element {
    let login_info = use_atom_state(&cx, LOGIN_INFO);
    let shown_login_info = match login_info.get() {
        Some(info) => rsx!(
            form {
                action: "/auth/logout",
                method: "POST",
                span {
                    class: "navbar-text",
                    "{info.name}"
                }
                button {
                    "type": "submit",
                    class: "ms-2 btn btn-danger",
                    "Log out"
                }
            }
        ),
        None => rsx!(
            span { class: "navbar-text", "Not signed in" }
        ),
    };
    let theme = appearance::use_theme(&cx);
    cx.render(rsx!(
        nav {
            class: "navbar navbar-expand-lg navbar-{theme.bg} {theme.bg_class} animate-colors",
            div {
                class: "container",
                a { class: "navbar-brand", "Hecate" }
                button {
                    "aria-controls": "basic-navbar-nav",
                    "aria-label": "Toggle navigation",
                    "type": "button",
                    class: "navbar-toggler",
                    span { class: "navbar-toggler-icon" }
                }
                div {
                    class: "navbar-collapse collapse",
                    id: "basic-navbar-nav",
                    div {
                        class: "me-auto navbar-nav",
                        a {
                            href: "/",
                            role: "button",
                            class: "nav-link",
                            tabindex: "0",
                            "Home"
                        }
                    }
                }
                div {
                    class: "justify-content-end navbar-collapse collapse",
                    shown_login_info,
                    ThemeToggleButton {
                        class: "mx-2",
                    },
                }
            }
        }
    ))
}
