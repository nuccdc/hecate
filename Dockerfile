FROM registry.gitlab.com/nuccdc/hecate/cached-deps:latest as builder
WORKDIR /hecate

COPY . .
RUN cargo build --release
RUN trunk --config frontend/Trunk.toml build --release

FROM debian:buster-slim
WORKDIR /hecate
EXPOSE 80
RUN apt update -y && apt install -y ca-certificates
COPY --from=builder /hecate/target/release/hecate .
COPY --from=builder /hecate/frontend/dist ./frontend/dist
COPY ./static ./static
ENTRYPOINT ["/hecate/hecate"]
