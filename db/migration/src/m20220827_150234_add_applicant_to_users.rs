use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let col_add = Table::alter()
            .table(Users::Table)
            .add_column(
                ColumnDef::new(Users::Applicant)
                    .boolean()
                    .not_null()
                    .default(false),
            )
            .to_owned();

        manager.alter_table(col_add).await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let col_remove = Table::alter()
            .table(Users::Table)
            .drop_column(Alias::new("applicant"))
            .to_owned();

        manager.alter_table(col_remove).await
    }
}

#[derive(Iden)]
enum Users {
    Table,
    Applicant,
}
