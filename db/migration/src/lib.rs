pub use sea_orm_migration::prelude::*;
use sea_orm_migration::sea_orm::DatabaseConnection;

pub mod m20220101_000001_create_users;
mod m20220827_150234_add_applicant_to_users;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220101_000001_create_users::Migration),
            Box::new(m20220827_150234_add_applicant_to_users::Migration),
        ]
    }
}

pub async fn run_migrations(db_conn: &DatabaseConnection) -> Result<(), DbErr> {
    Migrator::up(db_conn, None).await
}
