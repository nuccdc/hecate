FROM rust:1.62
WORKDIR /hecate

RUN cargo init
COPY Cargo.lock .
COPY Cargo.toml .

RUN cargo init --lib db/entity
COPY ./db/entity/Cargo.toml db/entity/

RUN cargo init --lib db/migration
COPY ./db/migration/Cargo.toml db/migration/

RUN cargo init --lib api/
COPY ./api/Cargo.toml api/

RUN cargo init --bin frontend/
COPY ./frontend/Cargo.toml frontend/
COPY ./frontend/Trunk.toml frontend/
COPY ./frontend/index.html frontend/

RUN mkdir -pv /cache/cargo
ENV CARGO_HOME="/cache/cargo/" PATH=/cache/cargo/bin:$PATH

RUN cargo fetch --locked
RUN cargo install trunk
RUN rustup target add wasm32-unknown-unknown
RUN trunk --config frontend/Trunk.toml build && rm -rf frontend/dist
RUN cargo build --release -p migration -p entity -p hecate -p api && cargo clean --release -p hecate -p migration -p entity -p api
