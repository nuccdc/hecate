use rocket::{fairing::Fairing, State};
use sea_orm::DatabaseConnection;

use crate::util;

pub type Connection = sea_orm::DatabaseConnection;
pub type ConnectionState = State<Connection>;

async fn setup_connection() -> DatabaseConnection {
    sea_orm::Database::connect(util::required_env("DATABASE_URL"))
        .await
        .expect("error setting up database connection")
}

struct DbFairing {}

#[rocket::async_trait]
impl Fairing for DbFairing {
    fn info(&self) -> rocket::fairing::Info {
        rocket::fairing::Info {
            kind: rocket::fairing::Kind::Ignite,
            name: "Database",
        }
    }

    async fn on_ignite(&self, rocket: rocket::Rocket<rocket::Build>) -> rocket::fairing::Result {
        let db_conn = setup_connection().await;
        log::info!("Running migrations...");
        match migration::run_migrations(&db_conn).await {
            Ok(_) => Ok(rocket.manage(db_conn)),
            Err(e) => panic!("Could not run migrations: {}", e),
        }
    }
}

pub fn fairing() -> impl Fairing {
    DbFairing {}
}
