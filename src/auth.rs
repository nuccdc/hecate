//! OAuth2 authentication.

use crate::{db, util};
use log::warn;
use migration::{DbErr, OnConflict};
use rocket::{
    fairing::Fairing,
    figment::Figment,
    get,
    http::{Cookie, CookieJar, SameSite, Status},
    post,
    request::{FromRequest, Outcome},
    response::Redirect,
    routes, Request,
};
use rocket_oauth2::{OAuth2, TokenResponse};
use sea_orm::{ActiveValue, EntityTrait};
use serde::Deserialize;
use std::{env, string};

const GROUPS_SCOPE: &str = "https://nuccdc.club/claims/gitlabGroups";

struct Auth0;

#[derive(Debug)]
pub enum LoginError {
    NotLoggedIn,
    NoDbConnection,
    NoSuchUser,
    NotTeamMember,
}

pub struct AuthenticatedUser {
    pub model: entity::user::Model,
}

pub struct AuthenticatedTeamMember {
    pub model: entity::user::Model,
}

impl AuthenticatedUser {
    async fn create_or_update_by(
        db_conn: &db::ConnectionState,
        sub: &str,
        name: &str,
        picture: &str,
        team: bool,
        applicant: bool,
    ) -> Result<AuthenticatedUser, DbErr> {
        let u = entity::user::ActiveModel {
            sub: ActiveValue::set(sub.to_string()),
            name: ActiveValue::set(name.to_string()),
            picture: ActiveValue::set(picture.to_string()),
            team: ActiveValue::set(team),
            applicant: ActiveValue::set(applicant),
        };
        let on_conflict: &mut migration::OnConflict =
            &mut OnConflict::column(entity::user::Column::Sub);
        on_conflict.update_columns(vec![
            entity::user::Column::Name,
            entity::user::Column::Picture,
        ]);
        let insert = entity::user::Entity::insert(u.clone()).on_conflict(on_conflict.clone());
        let res = insert
            .exec(db_conn.inner())
            .await
            .map(|_| AuthenticatedUser {
                model: entity::user::Model {
                    sub: sub.to_owned(),
                    name: name.to_owned(),
                    picture: picture.to_owned(),
                    team,
                    applicant,
                },
            });
        res
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticatedUser {
    type Error = LoginError;
    async fn from_request(request: &'r Request<'_>) -> Outcome<AuthenticatedUser, LoginError> {
        let db_connection = match request.guard::<&db::ConnectionState>().await {
            Outcome::Success(db_conn) => db_conn,
            _ => {
                return Outcome::Failure((Status::InternalServerError, LoginError::NoDbConnection))
            }
        };
        match request.cookies().get_private("sub") {
            None => Outcome::Failure((Status::Unauthorized, LoginError::NotLoggedIn)),
            Some(cookie) => {
                let sub = cookie.value();
                let user_res = entity::user::Entity::find_by_id(sub.to_string())
                    .all(db_connection.inner())
                    .await;
                match user_res {
                    Ok(mut user) if user.len() > 0 => Outcome::Success(AuthenticatedUser {
                        model: user.remove(0),
                    }),
                    _ => Outcome::Failure((Status::Unauthorized, LoginError::NoSuchUser)),
                }
            }
        }
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticatedTeamMember {
    type Error = LoginError;

    async fn from_request(
        request: &'r Request<'_>,
    ) -> Outcome<AuthenticatedTeamMember, LoginError> {
        let user = match request.guard::<AuthenticatedUser>().await {
            Outcome::Success(u) => u,
            _ => return Outcome::Failure((Status::Unauthorized, LoginError::NotLoggedIn)),
        };
        if user.model.team {
            Outcome::Success(AuthenticatedTeamMember { model: user.model })
        } else {
            Outcome::Failure((Status::Unauthorized, LoginError::NotTeamMember))
        }
    }
}

#[post("/auth/login")]
fn login(oauth2: OAuth2<Auth0>, mut cookies: &CookieJar<'_>) -> Redirect {
    oauth2
        .get_redirect(&mut cookies, &["openid", "email", "profile", GROUPS_SCOPE])
        .unwrap()
}

#[post("/auth/login")]
async fn debug_login(db_conn: &db::ConnectionState, cookies: &CookieJar<'_>) -> Redirect {
    let user = AuthenticatedUser::create_or_update_by(
        db_conn,
        "dev_user_sub",
        "Dev User",
        "http://localhost:8000/img/logo192.png",
        true,
        false,
    )
    .await;
    match user {
        Ok(user) => {
            set_login_cookie(cookies, user);
        }
        Err(e) => {
            log::error!("Error logging in: {}", e);
        }
    };
    Redirect::to("/")
}

#[derive(Deserialize)]
struct IdTokenInfo {
    #[serde(rename = "https://nuccdc.club/claims/gitlabGroups")]
    groups: Vec<String>,

    name: String,

    picture: String,

    sub: String,
}

#[derive(Debug)]
enum TokenDecodeError {
    B64(base64::DecodeError),
    Str(string::FromUtf8Error),
    Existence,
    JsonType,
    Length,
    Serde(serde_json::Error),
}

fn b64d(item: &str) -> Result<String, TokenDecodeError> {
    let decoded = base64::decode(item).map_err(|e| TokenDecodeError::B64(e))?;
    let str_val = String::from_utf8(decoded).map_err(|e| TokenDecodeError::Str(e))?;
    Ok(str_val)
}

fn parse_id_token(token: TokenResponse<Auth0>) -> Result<IdTokenInfo, TokenDecodeError> {
    let id_token = token
        .as_value()
        .get("id_token")
        .ok_or(TokenDecodeError::Existence)?;
    let id_token_str = id_token.as_str().ok_or(TokenDecodeError::JsonType)?;

    let splat: Vec<&str> = id_token_str.split(".").collect();
    let id_part = splat.get(1).ok_or(TokenDecodeError::Length)?;
    let decoded = b64d(id_part)?;
    let id_token_info: IdTokenInfo =
        serde_json::from_str(&decoded).map_err(|e| TokenDecodeError::Serde(e))?;

    Ok(id_token_info)
}

fn set_login_cookie(cookies: &CookieJar<'_>, user: AuthenticatedUser) {
    cookies.add_private(
        Cookie::build("sub", user.model.sub)
            .secure(true)
            .same_site(SameSite::Strict)
            .finish(),
    );
}

#[get("/auth/login_callback")]
async fn login_callback(
    token: TokenResponse<Auth0>,
    cookies: &CookieJar<'_>,
    db_conn: &db::ConnectionState,
) -> Redirect {
    let id_token_info = match parse_id_token(token) {
        Ok(info) => info,
        Err(_) => return Redirect::to("/"),
    };

    let team = id_token_info.groups.contains(&String::from("nuccdc/team"));
    let applicant = id_token_info
        .groups
        .contains(&String::from("nuccdc/applicants"));

    let user_res = AuthenticatedUser::create_or_update_by(
        db_conn,
        &id_token_info.sub,
        &id_token_info.name,
        &id_token_info.picture,
        team,
        applicant,
    )
    .await;
    let user = match user_res {
        Ok(u) => u,
        Err(e) => {
            warn!("login error: {}", e);
            return Redirect::to("/");
        }
    };
    set_login_cookie(cookies, user);
    Redirect::to("/")
}

#[post("/auth/logout")]
fn logout(cookies: &CookieJar<'_>) -> Redirect {
    if let Some(sub_cookie) = cookies.get_private("sub") {
        cookies.remove_private(sub_cookie);
    }
    Redirect::to("/")
}

fn configure_oauth(config: Figment) -> Figment {
    config
        .merge((
            "oauth.auth0.client_id",
            util::required_env("OAUTH_CLIENT_ID"),
        ))
        .merge((
            "oauth.auth0.client_secret",
            util::required_env("OAUTH_CLIENT_SECRET"),
        ))
        .merge(("oauth.auth0.auth_uri", util::required_env("OAUTH_AUTH_URL")))
        .merge((
            "oauth.auth0.token_uri",
            util::required_env("OAUTH_TOKEN_URL"),
        ))
        .merge((
            "oauth.auth0.redirect_uri",
            util::required_env("OAUTH_REDIRECT_DOMAIN") + "/auth/login_callback",
        ))
}

struct AuthFairing {
    debug: bool,
}

#[rocket::async_trait]
impl Fairing for AuthFairing {
    fn info(&self) -> rocket::fairing::Info {
        rocket::fairing::Info {
            kind: rocket::fairing::Kind::Ignite,
            name: "Authentication",
        }
    }

    async fn on_ignite(&self, rocket: rocket::Rocket<rocket::Build>) -> rocket::fairing::Result {
        let oauth_fairing = OAuth2::<Auth0>::fairing("auth0");
        let res = if self.debug {
            rocket.mount("/", routes![debug_login, logout])
        } else {
            let new_cfg = configure_oauth(rocket.figment().clone());
            rocket
                .mount("/", routes![login, login_callback, logout])
                .attach(oauth_fairing)
                .configure(new_cfg)
        };
        Ok(res)
    }
}

pub fn fairing() -> impl Fairing {
    match env::var("DEBUG") {
        Ok(_) => AuthFairing { debug: true },
        Err(_) => AuthFairing { debug: false },
    }
}
