//! # Hecate Backend
//! This crate contains the web backend of the Hecate server.

use auth::AuthenticatedUser;
use rocket::{
    figment::{providers::Serialized, Figment},
    fs::FileServer,
    get, launch, routes,
    serde::json::Json,
};

use crate::auth::AuthenticatedTeamMember;

extern crate rocket;

pub mod auth;
pub mod db;
pub mod frontend;
pub mod util;

fn static_files() -> FileServer {
    FileServer::from("static").rank(20)
}

fn config() -> Figment {
    rocket::Config::figment()
        .merge(Serialized::default("log_level", "normal"))
        .merge(Serialized::default("address", "0.0.0.0").profile("release"))
        .merge(Serialized::default("port", 80).profile("release"))
}

#[get("/me")]
fn me(user: AuthenticatedUser) -> Json<api::Me> {
    Json(api::Me {
        name: user.model.name,
        team: user.model.team,
        applicant: user.model.applicant,
    })
}

#[get("/applicants")]
fn applicants(_user: AuthenticatedTeamMember) -> Json<Vec<api::Applicant>> {
    Json(vec![
        api::Applicant {
            name: String::from("Sample Applicant"),
            picture: String::from("/img/logo192.png"),
            progress: vec![
                String::from("did something"),
                String::from("did something else"),
            ],
            time_used_percentage: 80,
        },
        api::Applicant {
            name: String::from("Sample Applicant 2"),
            picture: String::from("/img/logo192.png"),
            progress: vec![
                String::from("did something different"),
                String::from("second applicant did a thing"),
            ],
            time_used_percentage: 20,
        },
    ])
}

/// Rocket entrypoint.
#[launch]
async fn rocket() -> _ {
    rocket::custom(config())
        .mount("/", static_files())
        .attach(db::fairing())
        .attach(auth::fairing())
        .attach(frontend::fairing())
        .mount("/api", routes![me, applicants])
}
