use std::io::Result;
use std::path::Path;

use rocket::{
    fairing::Fairing,
    fs::{FileServer, NamedFile},
};

struct Frontend {}

fn dist_dir_server() -> FileServer {
    FileServer::from("./frontend/dist").rank(10)
}

#[rocket::get("/<_..>", rank = 30)]
async fn fallback() -> Result<NamedFile> {
    NamedFile::open(Path::new("./frontend/dist").join("index.html")).await
}

#[rocket::async_trait]
impl Fairing for Frontend {
    fn info(&self) -> rocket::fairing::Info {
        rocket::fairing::Info {
            kind: rocket::fairing::Kind::Ignite,
            name: "Frontend",
        }
    }

    async fn on_ignite(&self, rocket: rocket::Rocket<rocket::Build>) -> rocket::fairing::Result {
        Ok(rocket
            .mount("/", dist_dir_server())
            .mount("/", rocket::routes![fallback]))
    }
}

pub fn fairing() -> impl Fairing {
    Frontend {}
}
