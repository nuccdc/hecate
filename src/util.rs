//! Utility functions.

use std::env;

/// Panics if the environment variable for the given name does not exist.
/// Otherwise, returns its value.
pub fn required_env(var_name: &str) -> String {
    match env::var(var_name) {
        Ok(s) => s,
        _ => panic!("You must set a value for the env var: {}", var_name),
    }
}

/// Panics if any of the environment variables for the given names do not exist.
pub fn required_envs(var_names: &[&str]) {
    for var_name in var_names {
        required_env(var_name);
    }
}
